$(document).mouseup(function (e) {
    var container = $(".over");

    if (container.has(e.target).length === 0) {
        container.hide();
        $(".over").css("display", "none");
        history.pushState(false, "", ".");
    }
});
function saveStorage() {
    localStorage.setItem("name", $("#name").val());
    localStorage.setItem("email", $("#email").val());
    localStorage.setItem("text", $("#text").val());
    localStorage.setItem("agree", $("#agree").prop("checked"));
}

function loadStorage() {
    if (localStorage.getItem("name") !== null)
        $("#name").val(localStorage.getItem("name"));
    if (localStorage.getItem("email") !== null)
        $("#email").val(localStorage.getItem("email"));
    if (localStorage.getItem("text") !== null)
        $("#text").val(localStorage.getItem("text"));
    if (localStorage.getItem("agree") !== null) {
        $("#agree").prop("checked", localStorage.getItem("agree") === "true");
        if ($("#agree").prop("checked"))
            $("#send").removeAttr("disabled");
    }
}
function clear() {
    localStorage.clear()
    $("#name").val("");
    $("#email").val("");
    $("#text").val("");
    $("#agree").val(false);
}
function openB() {
    $(".over").css("display", "flex");
    history.pushState(true, "", "./form");
}

function closeB() {
    $(".over").css("display", "none");
    history.pushState(false, "", ".");
}
function agreeB() {
    if(this.checked)
        $("#send").removeAttr("disabled");
    else
        $("#send").attr("disabled", "");
}
$(document).ready(function() {
    loadStorage();
    $("#open").click(openB);
    $("#close").click(closeB);
    $("#form").submit(function(check) {
        check.preventDefault();
        $(".over").css("display", "none");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/3Cz3cGkMcA",
            data: $(this).serialize(),
            success: function(response){
                if(response.status == "success"){
                    alert("Заявка принята");
                    clear();
                } else {
                    alert("Ошибка " + response.message);
                }
            }
        });
    });
    $("#agree").change(agreeB)
    $("#form").change(saveStorage);
    
    window.onpopstate = function(event) {
        if (event.state)
            $(".over").css("display", "flex");
        else
            $(".over").css("display", "none");
    };
})


